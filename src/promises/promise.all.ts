//--------------------------------------------------
export async function all(
    promises: Iterable<unknown>
): Promise<Array<unknown>> {
    const results = [];
    for (const p of promises) {
        results.push(await p);
    }
    return results;
}
