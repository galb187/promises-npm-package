export async function reduce(
    iterable: Iterable<unknown>,
    cb: (
        acc: unknown,
        current: unknown,
        index?: number,
        length?: number
    ) => Promise<unknown>,
    initial?: unknown
) {
    const iter = Array.from(iterable);
    let aggregator = iter[0];
    if (initial !== undefined)
        aggregator = initial;    

    for (const item of iterable) {
        aggregator = await cb(aggregator, item);
    }

    return aggregator;
}
