export async function mapParallel(
    iterable: Iterable<unknown>,
    cb: (item: unknown, index?: number, length?: number) => Promise<unknown>
): Promise<unknown> {
    const results = [];
    const pending = Array.from(iterable, (item) => cb(item));
    for (const p of pending) {
        results.push(await p);
    }
    return results;
}

//--------------------------------------------------

export async function mapSeries(
    iterable: Iterable<unknown>,
    cb: (item: unknown, index?: number, length?: number) => Promise<unknown>
): Promise<unknown> {
    const results = [];
    for (const item of iterable) {
        results.push(await cb(await item));
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------
