export async function each(
    iterable: Iterable<unknown>,
    cb: (item: unknown, index?: number, length?: number) => Promise<unknown>
): Promise<Array<unknown>> {
    const ans = [];
    for (const item of iterable) {
        ans.push(await cb(await item));
    }
    return ans;
}
