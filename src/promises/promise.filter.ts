export async function filterSeries(
    iterable: Iterable<unknown>,
    cb: (item: unknown, index?: number, length?: number) => Promise<boolean>
): Promise<unknown> {
    const results = [];
    for (const item of iterable) {
        if ((await cb(await item)) === true) results.push(await item);
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------

export async function filterParallel(
    iterable: Iterable<unknown>,
    cb: (item: unknown, index?: number, length?: number) => Promise<boolean>
): Promise<unknown> {
    const results = [];
    const pending = Array.from(iterable, (item) => cb(item));
    const iter = Array.from(iterable);
    for (const [i, p] of pending.entries()) {
        if ((await p) === true){
            results.push(iter[i]);
        } 
    }

    return typeof iterable === "string" ? results.join("") : results;
}
