// you have to use the Promise object
export async function race(
    promises: Array<Promise<unknown>>
): Promise<unknown> {
    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            resolve(await p);
        });
    });
}
//--------------------------------------------------

export async function some(
    promises: Array<Promise<unknown>>,
    num: number
): Promise<unknown> {
    const results: Array<unknown> = [];
    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            results.push(await p);
            if (results.length === num) resolve(results);
        });
    });
}
