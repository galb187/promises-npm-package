export interface mapIterable {
    [key: string]: unknown;
}

export async function props(promisesObj: mapIterable) {
    const results: mapIterable = {};
    for (const key in promisesObj) {
        results[key] = await promisesObj[key];
    }
    return results;
}
