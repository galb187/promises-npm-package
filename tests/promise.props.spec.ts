import {props, mapIterable} from  "../src/promises/promise.props";
import { expect } from "chai";
import { delay } from "../src/promises/promise.utils";

describe("Promise Props Testing", ()=>{
    context("#props",()=>{
        let pendingArr : mapIterable = {};
        before(()=>{
            const p1 = new Promise(res =>{
                setTimeout(()=>res(1), 100);
            });
            const p2 = new Promise(res =>{
                setTimeout(()=>res(2), 100);
            });
            const p3 = new Promise(res =>{
                setTimeout(()=>res(3), 100);
            });
            pendingArr = {p1,p2,p3};
        });

        it("should exist", ()=>{
            expect(props).to.be.instanceOf(Function);
        });

        it("should return object with resolved values",async ()=>{
            const actual = {"p1":1,"p2":2,"p3":3};
            expect(actual).to.be.deep.equal(await props(pendingArr));
        });

        it("should catch error",async ()=>{
            const p1 = new Promise(res =>{
                setTimeout(()=>res(1), 1000);
            });
            const p2 = new Promise(res =>{
                setTimeout(()=>res(2), 1000);
            });
            const p3 = new Promise((res,rej) => {
                setTimeout(()=> res(3), 1000);
            });
            const f = async function(){
                throw new Error("throwing error");
            };
            const p4 = f();
            const arr = {p1,p2,p3,p4};
            try{
                expect(await props(arr)).to.throw("throwing error");
            }catch(err:unknown){
                console.log("error caught");
            }
        });
    });
});