import {mapParallel, mapSeries} from  "../src/promises/promise.map";
import { expect } from "chai";
import {delay} from "../src/promises/promise.utils";

describe("Promise Map Testing", ()=>{
    context("#mapSeries",()=>{
        it("should exist", ()=>{
            expect(mapSeries).to.be.instanceOf(Function);
        });

        it("should return array of resolved values modified by callback",async ()=>{
            const ms =100;
            const isOdd = async function (x: any) {
                await delay(ms);
                return x * 2;
            };
            const arr = [1, 2, 3, 4];
            const startTime=Date.now();
            const mappedArr = await mapSeries(arr, isOdd);
            const currTime=Date.now();
            console.log(currTime-startTime);
            expect(currTime-startTime).to.gte(ms*arr.length);
            expect(mappedArr).to.be.deep.equal([2,4,6,8]);
        });


        it("should throw an error an catch it", async ()=>{
            const ms =100;
            const isOdd = async function (x: any) {
                await delay(ms);
                if(x===6){
                    throw new Error("throwing error");
                }
                return x * 2;
            };
            const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            try{
                expect(await mapSeries(arr, isOdd)).to.throw("throwing error");
            }catch(err:unknown){
                console.log("error caught");
            }
        });
    });


    context("#mapParallel",()=>{
        
        it("should exist", ()=>{
            expect(mapParallel).to.be.instanceOf(Function);
        });

        it("should return array of resolved values modified by callback",async ()=>{
            const ms =100;
            const isOdd = async function (x: any) {
                await delay(ms);
                return x * 2 ;
            };
            const arr = [1, 2, 3, 4];
            const startTime=Date.now();
            const mappedArr = await mapParallel(arr, isOdd);
            const currTime=Date.now();
            console.log(currTime-startTime);
            expect(currTime-startTime).to.lte(ms*arr.length);
            expect(mappedArr).to.be.deep.equal([2,4,6,8]);
        });
    });
});