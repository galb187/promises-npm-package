import {each} from  "../src/promises/promise.each";
import { expect } from "chai";

describe("Promise Each Testing", ()=>{
    context("#each",()=>{
        it("should exist", ()=>{
            expect(each).to.be.instanceOf(Function);
        });

        it("should return array of resolved values",async ()=>{
            const actual = [2,4,6];
            const p1 = new Promise(res =>{
                setTimeout(()=>res(1), 1000);
            });
            const p2 = new Promise(res =>{
                setTimeout(()=>res(2), 1000);
            });
            const p3 = new Promise(res =>{
                setTimeout(()=>res(3), 1000);
            });
            const arr = [p1,p2,p3];
            const cb = async (item:unknown)=>{
                return (item as number) * 2;
            };
            expect(actual).to.be.deep.equal(await each(arr,cb));
        });
    });
});