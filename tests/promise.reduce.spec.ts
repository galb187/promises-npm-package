import {reduce} from  "../src/promises/promise.reduce";
import { expect } from "chai";
import {delay} from "../src/promises/promise.utils";

describe("Promise Reduce Testing", ()=>{
    context("#reduce",()=>{
        it("should exist", ()=>{
            expect(reduce).to.be.instanceOf(Function);
        });

        it("should return sum of element in array",async ()=>{
            const sumAll = async (total:unknown, num:unknown) => {
                            await delay(100);
                            (total as number) += (num as number);
                            return total;
                        };
            const arr = [1, 2, 3, 4];
            const result = await reduce(arr, sumAll, 0);
            expect(result).to.be.equal(10);
        });
    });
});