import {all} from  "../src/promises/promise.all";
import { expect } from "chai";

describe("Promise All Testing", ()=>{
    context("#all",()=>{
        it("should exist", ()=>{
            expect(all).to.be.instanceOf(Function);
        });

        it("should return array of resolved values",async ()=>{
            const actual = [1,2,3];
            const p1 = new Promise(res =>{
                setTimeout(()=>res(1), 1000);
            });
            const p2 = new Promise(res =>{
                setTimeout(()=>res(2), 1000);
            });
            const p3 = new Promise(res =>{
                setTimeout(()=>res(3), 1000);
            });
            const arr = [p1,p2,p3];
            expect(actual).to.be.deep.equal(await all(arr));
        });

        it("should catch error",async ()=>{
            const p1 = new Promise(res =>{
                setTimeout(()=>res(1), 1000);
            });
            const p2 = new Promise(res =>{
                setTimeout(()=>res(2), 1000);
            });
            const p3 = new Promise((res,rej) => {
                setTimeout(()=> res(3), 1000);
            });
            const f = async function(){
                throw new Error("throwing error");
            };
            const p4 = f();
            const arr = [p1,p2,p3,p4];
            try{
                expect(await all(arr)).to.throw("throwing error");
            }catch(err:unknown){
                console.log("error caught");
            }
        });

    });
});