import {race, some} from  "../src/promises/promise.race";
import { expect } from "chai";
import {echo, delay} from "../src/promises/promise.utils";

describe("Promise Race/Some Testing", ()=>{
    context("#race",()=>{
        it("should exist", ()=>{
            expect(race).to.be.instanceOf(Function);
        });

        it("should return first promise that resolves its value",async ()=>{
            const p1 = echo("first",150); 
            const p2 = echo("sec",50); 
            const p3 = echo("third",250); 
            const p4 = echo("forth",100);            
            const startTime=Date.now();
            const fastest = await race([p1,p2,p3,p4]);
            const currTime=Date.now();
            expect(currTime-startTime).to.lte(70);
            expect(fastest).to.be.equal("sec");
        });

        it("should catch error",async ()=>{
            const p1 = echo("first",150); 
            const p2 = echo("sec",50); 
            const p3 = echo("third",250); 
            const p4 = echo("forth",100);            
            const startTime=Date.now();
            const fastest = await race([p1,p2,p3,p4]);
            const currTime=Date.now();
            expect(currTime-startTime).to.lte(70);
            expect(fastest).to.be.equal("sec");
        });
    });


    context("#some",()=>{
        it("should exist", ()=>{
            expect(some).to.be.instanceOf(Function);
        });

        it("should return the 2 fastest promises that resolved",async ()=>{
            const p1 = echo("first",150); 
            const p2 = echo("sec",50); 
            const p3 = echo("third",250); 
            const p4 = echo("forth",100);            
            const startTime=Date.now();
            const fastest = await some([p1,p2,p3,p4],2);
            const currTime=Date.now();
            expect(currTime-startTime).to.lte(150);
            expect(fastest).to.be.deep.equal(["sec","forth"]);
        });
    });
});