import {filterParallel, filterSeries} from  "../src/promises/promise.filter";
import { expect } from "chai";
import { delay } from "../src/promises/promise.utils";

describe("Promise Filter Testing", ()=>{
    context("#filterSeries",()=>{
        // let pendingArr : Promise<unknown> [] = [];
        // before(()=>{
        //     const p1 = new Promise(res =>{
        //         setTimeout(()=>res(1), 1000);
        //     });
        //     const p2 = new Promise(res =>{
        //         setTimeout(()=>res(2), 1000);
        //     });
        //     const p3 = new Promise(res =>{
        //         setTimeout(()=>res(3), 1000);
        //     });
        //     pendingArr = [p1,p2,p3];
        // });

        it("should exist", ()=>{
            expect(filterSeries).to.be.instanceOf(Function);
        });

        it("should return array of filtered results by callback",async ()=>{
            const ms =100;
            const isOdd = async function (x: any) {
                await delay(ms);
                return x % 2 === 0;
            };
            const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            const startTime=Date.now();
            const filterArr = await filterSeries(arr, isOdd);
            const currTime=Date.now();
            console.log(currTime-startTime);
            expect(currTime-startTime).to.gte(ms*arr.length);
            expect(filterArr).to.be.deep.equal([2,4,6,8,10]);
        });

        it("should throw an error an catch it", async ()=>{
            const ms =100;
            const isOdd = async function (x: any) {
                await delay(ms);
                if(x===6){
                    throw new Error("throwing error");
                }
                return x % 2 === 0;
            };
            const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            try{
                expect(await filterSeries(arr, isOdd)).to.throw("throwing error");
            }catch(err:unknown){
                console.log("error caught");
            }
        });
    });


    context("#filterParallel",()=>{        
        it("should exist", ()=>{
            expect(filterParallel).to.be.instanceOf(Function);
        });

        it("should return array of filtered results by callback",async ()=>{
            const ms =100;
            const isOdd = async function (x: any) {
                await delay(ms);
                return x % 2 === 0;
            };
            const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            const startTime=Date.now();
            const filterArr = await filterParallel(arr, isOdd);
            const currTime=Date.now();
            console.log(currTime-startTime);
            expect(currTime-startTime).to.lte(ms*arr.length);
            expect(filterArr).to.be.deep.equal([2,4,6,8,10]);
        });
    });
});