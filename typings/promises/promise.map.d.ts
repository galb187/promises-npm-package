export declare function mapParallel(iterable: Iterable<unknown>, cb: (item: unknown, index?: number, length?: number) => Promise<unknown>): Promise<unknown>;
export declare function mapSeries(iterable: Iterable<unknown>, cb: (item: unknown, index?: number, length?: number) => Promise<unknown>): Promise<unknown>;
