export declare function race(promises: Array<Promise<unknown>>): Promise<unknown>;
export declare function some(promises: Array<Promise<unknown>>, num: number): Promise<unknown>;
