export declare function each(iterable: Iterable<unknown>, cb: (item: unknown, index?: number, length?: number) => Promise<unknown>): Promise<Array<unknown>>;
