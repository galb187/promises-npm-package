export interface mapIterable {
    [key: string]: unknown;
}
export declare function props(promisesObj: mapIterable): Promise<mapIterable>;
