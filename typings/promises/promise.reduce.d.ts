export declare function reduce(iterable: Iterable<unknown>, cb: (acc: unknown, current: unknown, index?: number, length?: number) => Promise<unknown>, initial?: unknown): Promise<unknown>;
