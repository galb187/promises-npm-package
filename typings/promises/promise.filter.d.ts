export declare function filterSeries(iterable: Iterable<unknown>, cb: (item: unknown, index?: number, length?: number) => Promise<boolean>): Promise<unknown>;
export declare function filterParallel(iterable: Iterable<unknown>, cb: (item: unknown, index?: number, length?: number) => Promise<boolean>): Promise<unknown>;
